import {DefaultCrudRepository} from '@loopback/repository';
import {SimpleModel} from '../models';
import {MysqlDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class SimpleModelRepository extends DefaultCrudRepository<
  SimpleModel,
  typeof SimpleModel.prototype.id
> {
  constructor(
    @inject('datasources.mysql') dataSource: MysqlDataSource,
  ) {
    super(SimpleModel, dataSource);
  }
}
