import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getFilterSchemaFor,
  getWhereSchemaFor,
  patch,
  put,
  del,
  requestBody,
} from '@loopback/rest';
import {SimpleModel} from '../models';
import {SimpleModelRepository} from '../repositories';

export class SimpleModelController {
  constructor(
    @repository(SimpleModelRepository)
    public simpleModelRepository : SimpleModelRepository,
  ) {}

  @post('/simple-models', {
    responses: {
      '200': {
        description: 'SimpleModel model instance',
        content: {'application/json': {schema: {'x-ts-type': SimpleModel}}},
      },
    },
  })
  async create(@requestBody() simpleModel: SimpleModel): Promise<SimpleModel> {
    return await this.simpleModelRepository.create(simpleModel);
  }

  @get('/simple-models/count', {
    responses: {
      '200': {
        description: 'SimpleModel model count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async count(
    @param.query.object('where', getWhereSchemaFor(SimpleModel)) where?: Where,
  ): Promise<Count> {
    return await this.simpleModelRepository.count(where);
  }

  @get('/simple-models', {
    responses: {
      '200': {
        description: 'Array of SimpleModel model instances',
        content: {
          'application/json': {
            schema: {type: 'array', items: {'x-ts-type': SimpleModel}},
          },
        },
      },
    },
  })
  async find(
    @param.query.object('filter', getFilterSchemaFor(SimpleModel)) filter?: Filter,
  ): Promise<SimpleModel[]> {
    return await this.simpleModelRepository.find(filter);
  }

  @patch('/simple-models', {
    responses: {
      '200': {
        description: 'SimpleModel PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async updateAll(
    @requestBody() simpleModel: SimpleModel,
    @param.query.object('where', getWhereSchemaFor(SimpleModel)) where?: Where,
  ): Promise<Count> {
    return await this.simpleModelRepository.updateAll(simpleModel, where);
  }

  @get('/simple-models/{id}', {
    responses: {
      '200': {
        description: 'SimpleModel model instance',
        content: {'application/json': {schema: {'x-ts-type': SimpleModel}}},
      },
    },
  })
  async findById(@param.path.number('id') id: number): Promise<SimpleModel> {
    return await this.simpleModelRepository.findById(id);
  }

  @patch('/simple-models/{id}', {
    responses: {
      '204': {
        description: 'SimpleModel PATCH success',
      },
    },
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody() simpleModel: SimpleModel,
  ): Promise<void> {
    await this.simpleModelRepository.updateById(id, simpleModel);
  }

  @put('/simple-models/{id}', {
    responses: {
      '204': {
        description: 'SimpleModel PUT success',
      },
    },
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() simpleModel: SimpleModel,
  ): Promise<void> {
    await this.simpleModelRepository.replaceById(id, simpleModel);
  }

  @del('/simple-models/{id}', {
    responses: {
      '204': {
        description: 'SimpleModel DELETE success',
      },
    },
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.simpleModelRepository.deleteById(id);
  }
}
