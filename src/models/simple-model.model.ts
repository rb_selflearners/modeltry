import {Entity, model, property} from '@loopback/repository';

@model()
export class SimpleModel extends Entity {
  @property({
    type: 'number',
    id: true,
  })
  id?: number;

  @property({
    type: 'string',
    required: true,
  })
  FirstField: string;

  @property({
    type: 'number',
  })
  SecondField?: number;

  @property({
    type: 'number',
  })
  ThirdField?: number;


  constructor(data?: Partial<SimpleModel>) {
    super(data);
  }
}
